package br.com.lgovea.business;

import br.com.lgovea.model.Funcionario;

public abstract class Imposto {
	private Funcionario funcionario;

	public Imposto(Funcionario funcionario) {
		super();
		this.funcionario = funcionario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}
}
