package br.com.lgovea.business;

import java.math.BigDecimal;

import br.com.lgovea.model.Funcionario;

public class Inss extends Imposto implements Desconto{
	
	private static final BigDecimal ALIQUOTA_PRIMEIRA_FAIXA = BigDecimal.valueOf(0.075);
	private static final BigDecimal ALIQUOTA_SEGUNDA_FAIXA = BigDecimal.valueOf(0.09);
	private static final BigDecimal ALIQUOTA_TERCEIRA_FAIXA = BigDecimal.valueOf(0.12);
	private static final BigDecimal ALIQUOTA_QUARTA_FAIXA = BigDecimal.valueOf(0.14);
	private static final BigDecimal TETO_INSS = BigDecimal.valueOf(6433.57);
	
	
	private static final BigDecimal VALOR_ABATIMENTO_SEGUNDA_FAIXA = new BigDecimal(16.5);
	private static final BigDecimal VALOR_ABATIMENTO_TERCEIRA_FAIXA = new BigDecimal(82.61);
	private static final BigDecimal VALOR_ABATIMENTO_QUARTA_FAIXA = new BigDecimal(148.72);

	public Inss(Funcionario funcionario) {
		super(funcionario);
	}

	@Override
	public BigDecimal calcularDesconto() {
		BigDecimal salario = this.getFuncionario().getSalarioBruto();
		if(salario.compareTo(BigDecimal.valueOf(1100.00)) <= 0) {
			return salario.multiply(ALIQUOTA_PRIMEIRA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else if(salario.compareTo(BigDecimal.valueOf(2203.48)) <= 0) {
			return salario.multiply(ALIQUOTA_SEGUNDA_FAIXA).subtract(VALOR_ABATIMENTO_SEGUNDA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else if(salario.compareTo(BigDecimal.valueOf(3305.22)) <= 0) {
			return salario.multiply(ALIQUOTA_TERCEIRA_FAIXA).subtract(VALOR_ABATIMENTO_TERCEIRA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else if(salario.compareTo(TETO_INSS) <= 0) {
			return salario.multiply(ALIQUOTA_QUARTA_FAIXA).subtract(VALOR_ABATIMENTO_QUARTA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else {
			return TETO_INSS.multiply(ALIQUOTA_QUARTA_FAIXA).subtract(VALOR_ABATIMENTO_QUARTA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	

}
