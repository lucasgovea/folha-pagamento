package br.com.lgovea.business.exception;

public class DependenteException extends Exception{
	
	private static final long serialVersionUID = 8049943070216308208L;

	public DependenteException() {
		super();
	}
	
	public DependenteException(String msg) {
		super(msg);
	}
}
