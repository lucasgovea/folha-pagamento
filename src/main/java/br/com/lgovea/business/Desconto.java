package br.com.lgovea.business;

import java.math.BigDecimal;

public interface Desconto {
	
	BigDecimal calcularDesconto();
}
