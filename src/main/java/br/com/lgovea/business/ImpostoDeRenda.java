package br.com.lgovea.business;


import java.math.BigDecimal;

import br.com.lgovea.model.Funcionario;

public class ImpostoDeRenda extends Imposto implements Desconto{
	
	private static final BigDecimal ALIQUOTA_PRIMEIRA_FAIXA = BigDecimal.valueOf(.075);
	private static final BigDecimal ALIQUOTA_SEGUNDA_FAIXA = BigDecimal.valueOf(0.15);
	private static final BigDecimal ALIQUOTA_TERCEIRA_FAIXA = BigDecimal.valueOf(0.225);
	private static final BigDecimal ALIQUOTA_QUARTA_FAIXA = BigDecimal.valueOf(0.275);
	private static final BigDecimal VALOR_ABATIMENTO_POR_DEPENDENTE = BigDecimal.valueOf(189.59);
	
	private static final BigDecimal VALOR_ABATIMENTO_PRIMEIRA_FAIXA = BigDecimal.valueOf(142.80);
	private static final BigDecimal VALOR_ABATIMENTO_SEGUNDA_FAIXA = BigDecimal.valueOf(354.80);
	private static final BigDecimal VALOR_ABATIMENTO_TERCEIRA_FAIXA = BigDecimal.valueOf(636.13);
	private static final BigDecimal VALOR_ABATIMENTO_QUARTA_FAIXA = BigDecimal.valueOf(869.36);
	

	public ImpostoDeRenda(Funcionario funcionario) {
		super(funcionario);
	}

	@Override
	public BigDecimal calcularDesconto() {
		BigDecimal qntDependente = BigDecimal.valueOf(this.getFuncionario().getDependentes().size());
		
		BigDecimal salarioBase = this.getFuncionario().getSalarioBruto().subtract(this.getFuncionario().getDescontoInss())
				.subtract(VALOR_ABATIMENTO_POR_DEPENDENTE.multiply(qntDependente)).setScale(3, BigDecimal.ROUND_HALF_UP);
		
		if(salarioBase.compareTo(new BigDecimal(1903.98)) <= 0) {
			return BigDecimal.ZERO;
		}else if(salarioBase.compareTo(new BigDecimal(2826.65)) <= 0) {
			return salarioBase.multiply(ALIQUOTA_PRIMEIRA_FAIXA).subtract(VALOR_ABATIMENTO_PRIMEIRA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else if(salarioBase.compareTo(new BigDecimal(3751.05)) <= 0) {
			return salarioBase.multiply(ALIQUOTA_SEGUNDA_FAIXA).subtract(VALOR_ABATIMENTO_SEGUNDA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else if(salarioBase.compareTo(new BigDecimal(4664.68)) <= 0) {
			return salarioBase.multiply(ALIQUOTA_TERCEIRA_FAIXA).subtract(VALOR_ABATIMENTO_TERCEIRA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else {
			return salarioBase.multiply(ALIQUOTA_QUARTA_FAIXA).subtract(VALOR_ABATIMENTO_QUARTA_FAIXA).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	

}
