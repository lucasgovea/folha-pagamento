package br.com.lgovea.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.lgovea.model.Dependente;


@Transactional
public interface IDependenteRepository extends CrudRepository<Dependente, Long>{
	
	
}
