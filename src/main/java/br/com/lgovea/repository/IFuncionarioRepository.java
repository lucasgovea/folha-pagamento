package br.com.lgovea.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.lgovea.model.Funcionario;


@Transactional
public interface IFuncionarioRepository extends CrudRepository<Funcionario, Long>{
	Set<Funcionario> findAll();
}
