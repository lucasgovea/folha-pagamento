package br.com.lgovea.enumerator;

public enum Parentesco {
	FILHO("Filho"), SOBRINHO("Sobrinho"), OUTROS("Outros");

	
	private final String descricao;
	
	private Parentesco(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	

}
