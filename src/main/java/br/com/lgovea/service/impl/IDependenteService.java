package br.com.lgovea.service.impl;

import java.util.Optional;

import br.com.lgovea.model.Dependente;

public interface IDependenteService {
	Optional<Dependente> obterDependentePorId(Long id);
	boolean apagarDependentePorId(Long id);
	Dependente editarDependentePorId(Dependente dependente);
	Dependente incluirDependente(Dependente dependente);
}
