package br.com.lgovea.service.impl;

import java.util.Set;

import br.com.lgovea.business.exception.DependenteException;
import br.com.lgovea.model.Funcionario;

public interface IFolhaPagamentoService {
	Set<Funcionario> processarFolha() throws DependenteException;
}
