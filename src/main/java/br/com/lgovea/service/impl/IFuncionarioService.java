package br.com.lgovea.service.impl;

import java.util.Optional;
import java.util.Set;

import br.com.lgovea.model.Funcionario;

public interface IFuncionarioService {
	Set<Funcionario> obterFuncionarios();
	Optional<Funcionario> obterFuncionarioPorId(Long id);
	boolean apagarFuncionarioPorId(Long id);
	Funcionario editarFuncionario(Funcionario funcionario);
	Funcionario incluirFuncionario(Funcionario funcionario);
}
