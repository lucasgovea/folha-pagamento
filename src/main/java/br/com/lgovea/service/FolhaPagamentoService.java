package br.com.lgovea.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lgovea.business.Desconto;
import br.com.lgovea.business.ImpostoDeRenda;
import br.com.lgovea.business.Inss;
import br.com.lgovea.model.Funcionario;
import br.com.lgovea.service.impl.IFolhaPagamentoService;
import br.com.lgovea.service.impl.IFuncionarioService;

@Service
public class FolhaPagamentoService implements IFolhaPagamentoService{
	
	@Autowired
	IFuncionarioService funcionarioService;
	
	public Set<Funcionario> processarFolha() {
		Set<Funcionario> funcionarios = funcionarioService.obterFuncionarios();
		
		for (Funcionario funcionario : funcionarios) {
			
			Desconto inss = new Inss(funcionario);
			funcionario.setDescontoInss(inss.calcularDesconto());
			
			Desconto ir = new ImpostoDeRenda(funcionario);
			funcionario.setDescontoIr(ir.calcularDesconto());
		}
		return funcionarios;
	}
	
}
