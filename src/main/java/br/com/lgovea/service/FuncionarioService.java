package br.com.lgovea.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lgovea.model.Funcionario;
import br.com.lgovea.repository.IFuncionarioRepository;
import br.com.lgovea.service.impl.IFuncionarioService;

@Service
public class FuncionarioService implements IFuncionarioService{

	@Autowired
	IFuncionarioRepository funcionarioRepo;
	
	@Override
	public Set<Funcionario> obterFuncionarios() {
		return funcionarioRepo.findAll();
	}

	@Override
	public Optional<Funcionario> obterFuncionarioPorId(Long id) {
		return funcionarioRepo.findById(id);
	}

	@Override
	public boolean apagarFuncionarioPorId(Long id) {
		if(funcionarioRepo.existsById(id)) {
			funcionarioRepo.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Funcionario editarFuncionario(Funcionario funcionario) {
		return funcionarioRepo.save(funcionario);
	}

	@Override
	public Funcionario incluirFuncionario(Funcionario funcionario) {
		return funcionarioRepo.save(funcionario);
	}
	
}
