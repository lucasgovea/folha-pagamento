package br.com.lgovea.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lgovea.model.Dependente;
import br.com.lgovea.repository.IDependenteRepository;
import br.com.lgovea.service.impl.IDependenteService;

@Service
public class DependenteService implements IDependenteService{

	@Autowired
	IDependenteRepository dependenteRepo;
	
	@Override
	public Optional<Dependente> obterDependentePorId(Long id) {
		return dependenteRepo.findById(id);
	}

	@Override
	public boolean apagarDependentePorId(Long id) {
		if(dependenteRepo.existsById(id)) {
			dependenteRepo.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Dependente editarDependentePorId(Dependente dependente) {
		Optional<Dependente> funcOpt = dependenteRepo.findById(dependente.getId());
		Dependente funcBd = null;
		if(funcOpt.isPresent()) {
			funcBd = funcOpt.get();
			funcBd.setCpf(dependente.getCpf());
			funcBd.setDataNascimento(dependente.getDataNascimento());
			funcBd.setNome(dependente.getNome());
			funcBd.setParentesco(dependente.getParentesco());
			dependenteRepo.save(funcBd);
		}
		return funcBd;
	}

	@Override
	public Dependente incluirDependente(Dependente dependente) {
		return dependenteRepo.save(dependente);
	}
	
}
