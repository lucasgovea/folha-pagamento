package br.com.lgovea.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
public class Funcionario extends Pessoa implements Serializable{
	
	private static final long serialVersionUID = -8247823670510562987L;
	
	@Column
	private BigDecimal salarioBruto;
	@Transient
	private BigDecimal descontoInss;
	@Transient
	private BigDecimal descontoIr;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="funcionario_id", nullable = false)
	private Set<Dependente> dependentes = new HashSet<>();
	
	public BigDecimal getSalarioBruto() {
		return salarioBruto;
	}
	public void setSalarioBruto(BigDecimal salarioBruto) {
		this.salarioBruto = salarioBruto;
	}
	
	public BigDecimal getDescontoInss() {
		return descontoInss;
	}
	public void setDescontoInss(BigDecimal descontoInss) {
		this.descontoInss = descontoInss;
	}
	
	public BigDecimal getDescontoIr() {
		return descontoIr;
	}
	public void setDescontoIr(BigDecimal descontoIr) {
		this.descontoIr = descontoIr;
	}
	
	public Set<Dependente> getDependentes() {
		return dependentes;
	}
	public void setDependentes(Set<Dependente> dependentes) {
		this.dependentes = dependentes;
	}
}
