package br.com.lgovea.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.lgovea.enumerator.Parentesco;

@Entity
public class Dependente extends Pessoa implements Serializable{

	private static final long serialVersionUID = -7729378628672426584L;

	@Column
	@Enumerated(EnumType.STRING)
	private Parentesco parentesco;
	

	public Parentesco getParentesco() {
		return parentesco;
	}

	public void setParentesco(Parentesco parentesco) {
		this.parentesco = parentesco;
	}
	
}
