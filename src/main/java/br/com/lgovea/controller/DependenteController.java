package br.com.lgovea.controller;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lgovea.model.Dependente;
import br.com.lgovea.service.impl.IDependenteService;

@RestController
@ExposesResourceFor(Dependente.class)
@RequestMapping(value = "/api/v1/dependente", produces = "application/json")
public class DependenteController {
	
	@Autowired
	IDependenteService dependenteService;
	
	@PostMapping
	public ResponseEntity<Dependente> incluir(@RequestBody Dependente novo) {
		Dependente d = dependenteService.incluirDependente(novo);
		createSelfLink(d);
		return new ResponseEntity<>(d, HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Dependente> obterPorId(@PathVariable Long id) {
		Optional<Dependente> depOpt = dependenteService.obterDependentePorId(id);
		if(depOpt.isPresent()) {
			createSelfLink(depOpt.get());
			return new ResponseEntity<>(depOpt.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/{id}")
	public void alterar(Dependente novo) {
		
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> apagar(@PathVariable Long id) {
		if(dependenteService.apagarDependentePorId(id)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	private void createSelfLink(Dependente dependente) {
		Link selfLink = WebMvcLinkBuilder.linkTo(DependenteController.class).slash(dependente.getId()).withSelfRel();
		dependente.add(selfLink);
	}
}
