package br.com.lgovea.controller;


import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lgovea.model.Funcionario;
import br.com.lgovea.service.impl.IFuncionarioService;

@RestController
@ExposesResourceFor(Funcionario.class)
@RequestMapping(value = "/api/v1/funcionario", produces = "application/json")
public class FuncionarioController {
	
	@Autowired
	IFuncionarioService funcionarioService;
	
	@PostMapping
	public ResponseEntity<Funcionario> incluir(@RequestBody Funcionario novo) {
		Funcionario f = funcionarioService.incluirFuncionario(novo);
		createSelfLink(f);
		return new ResponseEntity<>(f, HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<Set<Funcionario>> obter() {
		Set<Funcionario> funcionarios = funcionarioService.obterFuncionarios();
		funcionarios.forEach(f-> createSelfLinkInCollections(f));
		return new ResponseEntity<>(funcionarios, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Funcionario> obterPorId(@PathVariable Long id) {
		Optional<Funcionario> funcOpt = funcionarioService.obterFuncionarioPorId(id);
		if(funcOpt.isPresent()) {
			createSelfLink(funcOpt.get());
			return new ResponseEntity<>(funcOpt.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/{id}")
	public void alterar(@PathVariable Long id, @RequestBody Funcionario novo) {
		novo.setId(id);
		funcionarioService.editarFuncionario(novo);
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> apagar(@PathVariable Long id) {
		if(funcionarioService.apagarFuncionarioPorId(id)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	private void createSelfLink(Funcionario funcionario) {
		Link selfLink = WebMvcLinkBuilder.linkTo(FuncionarioController.class).slash(funcionario.getId()).withSelfRel();
		funcionario.getDependentes().forEach(d->{
			Link sl = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DependenteController.class)
					.obterPorId(d.getId())).withSelfRel();
			d.add(sl);
		});
		funcionario.add(selfLink);
	}
	
	private void createSelfLinkInCollections(Funcionario f) {
		Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(FuncionarioController.class).obterPorId(f.getId()))
				.withSelfRel().expand();
		f.getDependentes().forEach(d->{
			Link sl = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DependenteController.class)
					.obterPorId(d.getId())).withSelfRel();
			d.add(sl);
		});
		f.add(selfLink);
	}
	
	
	
}
