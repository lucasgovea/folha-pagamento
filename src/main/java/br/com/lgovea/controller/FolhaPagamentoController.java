package br.com.lgovea.controller;


import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lgovea.business.exception.DependenteException;
import br.com.lgovea.model.Funcionario;
import br.com.lgovea.service.impl.IFolhaPagamentoService;

@RestController
@RequestMapping(value = "/api/v1/folha-pagamento", produces = "application/json")
public class FolhaPagamentoController {
	
	@Autowired
	IFolhaPagamentoService folhaPagamentoService;
	
	@GetMapping
	public ResponseEntity<Set<Funcionario>> calcularFolha() {
		try {
			Set<Funcionario> funcionarios = folhaPagamentoService.processarFolha();
			funcionarios.forEach(f->this.createSelfLink(f));
			return new ResponseEntity<>(funcionarios, HttpStatus.OK);
		} catch (DependenteException e) {
			// TODO Auto-generated catch block
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	
	private void createSelfLink(Funcionario func) {
		Link selfLink = WebMvcLinkBuilder.linkTo(FuncionarioController.class).slash(func.getId()).withSelfRel();
		func.add(selfLink);
	}
}
